package com.m.ticket.service.ticketService.service;

import com.m.ticket.service.ticketService.domin.Bid;
import com.m.ticket.service.ticketService.domin.Client;
import com.m.ticket.service.ticketService.domin.Ticket;
import com.m.ticket.service.ticketService.dto.bid.BidCreateDTO;
import com.m.ticket.service.ticketService.dto.bid.BidReadDTO;
import com.m.ticket.service.ticketService.dto.bid.BidReadDTOExtended;
import com.m.ticket.service.ticketService.dto.bid.BidReadStatusDTO;
import com.m.ticket.service.ticketService.dto.ticket.TicketReadDTO;
import com.m.ticket.service.ticketService.enums.BidStatus;
import com.m.ticket.service.ticketService.exeption.EntityNotFoundException;
import com.m.ticket.service.ticketService.repository.BidRepository;
import com.m.ticket.service.ticketService.repository.ClientRepository;
import com.m.ticket.service.ticketService.repository.TicketRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.UUID;


@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest
public class BidServiceTest {

    @Autowired
    private BidRepository bidRepository;

    @Autowired
    private BidService bidService;

    @Autowired
    private TicketRepository ticketRepository;

    @Autowired
    private TicketService ticketService;

    @Autowired
    private ClientRepository clientRepository;

    @Test
    public void testCreateBid() {
        Instant instant = Instant.now().plus(1, ChronoUnit.DAYS);
        Ticket ticket = new Ticket();
        ticket.setDeparture(instant);
        ticket.setRouteNumber("92a");
        ticketRepository.save(ticket);
        TicketReadDTO ticketRead = ticketService
                .getTicketRouteAndTime(ticket.getDeparture(), ticket.getRouteNumber());
        Assert.assertNotNull(ticketRead.getDeparture());

        BidCreateDTO createDTO = new BidCreateDTO();
        createDTO.setRoute("92a");
        createDTO.setTimeDeparture(instant);
        BidReadDTO read = bidService.createBid(createDTO);
        Assert.assertNotNull(read.getId());

    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetBidStatusWrongId() {
        bidService.getBidStatus(UUID.randomUUID());
    }

    @Test
    public void testCreateBidWithoutTicket() {
        Instant instant = Instant.now().plus(1, ChronoUnit.DAYS);
        BidCreateDTO createDTO = new BidCreateDTO();
        createDTO.setRoute("92a");
        createDTO.setTimeDeparture(instant);
        createDTO.setClientId(UUID.randomUUID());
        BidReadDTO read = bidService.createBid(createDTO);
        Assert.assertNull(read.getId());

    }

    @Test
    public void testCreateBidWrongDeparture() {
        Instant instant = Instant.now().plus(1, ChronoUnit.DAYS);
        Ticket ticket = new Ticket();
        ticket.setDeparture(instant);
        ticket.setRouteNumber("92a");
        ticketRepository.save(ticket);
        TicketReadDTO ticketRead = ticketService
                .getTicketRouteAndTime(ticket.getDeparture(), ticket.getRouteNumber());
        Assert.assertNotNull(ticketRead.getDeparture());

        BidCreateDTO createDTO = new BidCreateDTO();
        createDTO.setRoute("92a");
        createDTO.setClientId(UUID.randomUUID());
        createDTO.setTimeDeparture(instant.plus(1, ChronoUnit.MINUTES));
        BidReadDTO read = bidService.createBid(createDTO);
        Assert.assertNull(read.getId());
    }

    @Test
    public void testGetBidStatus() {
        Instant instant = Instant.now();
        Bid bid = new Bid();
        bid.setRoute("92a");
        bid.setTimeDeparture(instant);
        bid.setBidStatus(BidStatus.PROCESSED_MADE);
        bid = bidRepository.save(bid);

        BidReadStatusDTO read = bidService.getBidStatus(bid.getId());
        Assert.assertEquals(bid.getBidStatus(), read.getStatus());
    }

    @Test
    public void testGetListBidByClientAndSort() {
        Instant instant = Instant.now();

        Client client = new Client();
        client = clientRepository.save(client);

        Ticket ticket = new Ticket();
        ticket.setDeparture(instant.minus(10, ChronoUnit.MINUTES));
        ticket.setRouteNumber("1a");
        ticket = ticketRepository.save(ticket);

        Ticket ticket1 = new Ticket();
        ticket1.setRouteNumber("3a");
        ticket1.setDeparture(instant.plus(40, ChronoUnit.MINUTES));
        ticket1 = ticketRepository.save(ticket1);

        Ticket ticket2 = new Ticket();
        ticket2.setDeparture(instant.plus(5, ChronoUnit.MINUTES));
        ticket2.setRouteNumber("99a");
        ticket2 = ticketRepository.save(ticket2);

        Ticket ticket3 = new Ticket();
        ticket3.setDeparture(instant.plus(1, ChronoUnit.DAYS));
        ticket3.setRouteNumber("1a");
        ticket3 = ticketRepository.save(ticket3);

        Bid bid = new Bid();
        bid.setTimeDeparture(ticket.getDeparture());
        bid.setClientId(client.getId());
        bid.setBidStatus(BidStatus.BID_ERROR);
        bid = bidRepository.save(bid);

        Bid bid1 = new Bid();
        bid1.setTimeDeparture(ticket1.getDeparture());
        bid1.setClientId(client.getId());
        bid1.setBidStatus(BidStatus.PROCESSED_MADE);
        bid1 = bidRepository.save(bid1);

        Bid bid2 = new Bid();
        bid2.setTimeDeparture(ticket2.getDeparture());
        bid2.setClientId(client.getId());
        bid2.setBidStatus(BidStatus.PROCESSED_MADE);
        bid2 = bidRepository.save(bid2);

        Bid bid3 = new Bid();
        bid3.setTimeDeparture(ticket3.getDeparture());
        bid3.setClientId(client.getId());
        bid3.setBidStatus(BidStatus.BID_ERROR);
        bid3 = bidRepository.save(bid3);

        List<BidReadDTOExtended> bidReadDTOExtendedList = bidService.getBidByClientIdAndSort(client.getId());

        Assert.assertTrue(bidReadDTOExtendedList.get(0).getTimeDeparture().equals(bid2.getTimeDeparture()));
        Assert.assertTrue(bidReadDTOExtendedList.get(1).getTimeDeparture().equals(bid1.getTimeDeparture()));
        Assert.assertTrue(bidReadDTOExtendedList.get(2).getTimeDeparture().equals(bid3.getTimeDeparture()));


    }


}