package com.m.ticket.service.ticketService.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.m.ticket.service.ticketService.dto.PayReadDTO;
import com.m.ticket.service.ticketService.enums.PayStatus;
import com.m.ticket.service.ticketService.service.PaymentService;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.UUID;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@WebMvcTest(PaymentController.class)
public class PaymentControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private PaymentService paymentService;

    @Test
    public void testGetPay() throws Exception {
        PayReadDTO pay = new PayReadDTO();
        pay.setPayStatus(PayStatus.PAYED);
        UUID id = UUID.randomUUID();

        Mockito.when(paymentService.pay(id)).thenReturn(pay);

        String resultJson = mvc.perform(get("/api/pay/{id}", id))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        PayReadDTO actual = objectMapper.readValue(resultJson, PayReadDTO.class);

        Assertions.assertThat(actual.getPayStatus()).isEqualToComparingFieldByField(pay.getPayStatus());


    }

}