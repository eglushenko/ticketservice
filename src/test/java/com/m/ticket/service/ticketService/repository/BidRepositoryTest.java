package com.m.ticket.service.ticketService.repository;

import com.m.ticket.service.ticketService.domin.Bid;
import com.m.ticket.service.ticketService.enums.BidStatus;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.Instant;
import java.util.UUID;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest
public class BidRepositoryTest {

    @Autowired
    BidRepository bidRepository;

    @Test
    public void testCreateBid() {
        Bid bid = new Bid();
        bid.setBidStatus(BidStatus.PROCESS);
        bid.setRoute("aaa");
        bid.setClientId(UUID.randomUUID());
        bid.setTimeDeparture(Instant.now());
        bidRepository.save(bid);

        String routAfterSave = bidRepository.findById(bid.getId()).get().getRoute();
        Assert.assertEquals(bid.getRoute(), routAfterSave);
    }

}