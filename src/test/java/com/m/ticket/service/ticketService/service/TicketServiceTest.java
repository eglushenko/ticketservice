package com.m.ticket.service.ticketService.service;

import com.m.ticket.service.ticketService.domin.Ticket;
import com.m.ticket.service.ticketService.dto.ticket.TicketReadDTO;
import com.m.ticket.service.ticketService.repository.TicketRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.Instant;


@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest
public class TicketServiceTest {

    @Autowired
    private TicketRepository ticketRepository;

    @Autowired
    TicketService ticketService;

    @Test
    public void testGetTicketByTimeAndRoute() {
        Instant in = Instant.now();
        Ticket ticket = new Ticket();
        ticket.setRouteNumber("1");
        ticket.setDeparture(in);
        ticket = ticketRepository.save(ticket);

        TicketReadDTO read = ticketService.getTicketRouteAndTime(in, ticket.getRouteNumber());

        Assert.assertEquals(ticket.getId(), read.getId());

    }

}