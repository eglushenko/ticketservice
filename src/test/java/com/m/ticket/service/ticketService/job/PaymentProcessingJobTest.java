package com.m.ticket.service.ticketService.job;

import com.m.ticket.service.ticketService.domin.Bid;
import com.m.ticket.service.ticketService.dto.bid.BidReadDTOExtended;
import com.m.ticket.service.ticketService.enums.BidStatus;
import com.m.ticket.service.ticketService.repository.BidRepository;
import com.m.ticket.service.ticketService.service.BidService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest
public class PaymentProcessingJobTest {

    @Autowired
    private BidRepository bidRepository;

    @Autowired
    private PaymentProcessingJob paymentProcessingJob;

    @Autowired
    private BidService bidService;

    @Test
    public void testPaymentProcessingJob() {
        Instant instant = Instant.now();
        List<UUID> uuidList = new ArrayList();
        for (int i = 0; i == 100; i++) {
            Bid bid = new Bid();
            bid.setBidStatus(BidStatus.PROCESS);
            bid.setTimeDeparture(instant);
            bid = bidRepository.save(bid);
            uuidList.add(bid.getId());
        }
        paymentProcessingJob.paymentJob();

        uuidList.forEach(o -> {
            BidReadDTOExtended bidReadDTOExtended = bidService.getBid(o);
            Assert.assertNotEquals(BidStatus.PROCESS, bidReadDTOExtended.getBidStatus());

        });


    }

}