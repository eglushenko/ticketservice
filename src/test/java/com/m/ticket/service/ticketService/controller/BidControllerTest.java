package com.m.ticket.service.ticketService.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.m.ticket.service.ticketService.domin.Bid;
import com.m.ticket.service.ticketService.dto.bid.BidCreateDTO;
import com.m.ticket.service.ticketService.dto.bid.BidReadDTO;
import com.m.ticket.service.ticketService.enums.BidStatus;
import com.m.ticket.service.ticketService.exeption.EntityNotFoundException;
import com.m.ticket.service.ticketService.service.BidService;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@WebMvcTest(BidController.class)
public class BidControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private BidService bidService;

    @Autowired
    private ObjectMapper objectMapper;


    @Test
    public void testCreateBid() throws Exception {
        BidCreateDTO create = new BidCreateDTO();
        create.setTimeDeparture(Instant.now().plus(10, ChronoUnit.MINUTES));
        create.setBidStatus(BidStatus.PROCESS);
        create.setRoute("aaa");
        create.setClientId(UUID.randomUUID());
        BidReadDTO read = new BidReadDTO();
        read.setId(UUID.randomUUID());

        Mockito.when(bidService.createBid(create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/bid")
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        BidReadDTO bidReadDTO = objectMapper.readValue(resultJson, BidReadDTO.class);
        Assertions.assertThat(bidReadDTO).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testGetBidWrongUUIDFormat() throws Exception {
        String resultJson = String.valueOf(mvc.perform(get("/api/v1/bid/smile"))
                .andReturn().getResponse().getStatus());
        Assert.assertTrue(resultJson.contains("400"));
    }

    @Test
    public void testGetBidWrongId() throws Exception {
        UUID wrongId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(Bid.class, wrongId);

        Mockito.when(bidService.getBid(wrongId)).thenThrow(exception);

        String resultJson = mvc.perform(get("/api/v1/bid/{id}", wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();
        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testGetBidStatusWrongId() throws Exception {
        UUID wrongId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(Bid.class, wrongId);

        Mockito.when(bidService.getBidStatus(wrongId)).thenThrow(exception);

        String resultJson = mvc.perform(get("/api/v1/bid/status/{id}", wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();
        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }


}