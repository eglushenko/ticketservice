package com.m.ticket.service.ticketService.service;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Component
public class ExecutorManager {

    private static int i = 1;

    public static final ExecutorService executorService = Executors.newFixedThreadPool(i);

    @Value("${payThreadCount}")
    public void setDatabase(String s) {
        Integer count = Integer.parseInt(s);
        i = count;
    }

}
