package com.m.ticket.service.ticketService.service;

import com.m.ticket.service.ticketService.dto.PayReadDTO;
import com.m.ticket.service.ticketService.enums.PayStatus;
import org.springframework.stereotype.Service;

import java.util.UUID;


@Service
public class PaymentService {

    public PayReadDTO pay(UUID id) {
        PayReadDTO read = new PayReadDTO();
        int a = (int) (Math.random() * 3);
        if (a == 0) {
            read.setPayStatus(PayStatus.ERROR);
        }
        if (a == 1) {
            read.setPayStatus(PayStatus.PAYED);
        }
        if (a == 2) {
            read.setPayStatus(PayStatus.PENDING);
        }
        return read;
    }
}
