package com.m.ticket.service.ticketService.service;


import com.m.ticket.service.ticketService.domin.Bid;
import com.m.ticket.service.ticketService.dto.bid.*;
import com.m.ticket.service.ticketService.dto.ticket.TicketReadDTO;
import com.m.ticket.service.ticketService.enums.BidStatus;
import com.m.ticket.service.ticketService.exeption.EntityNotFoundException;
import com.m.ticket.service.ticketService.repository.BidRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class BidService {

    @Autowired
    private BidRepository bidRepository;

    @Autowired
    private TicketService ticketService;

    public BidReadDTOExtended toReadDTO(Bid bid) {
        BidReadDTOExtended bidReadDTO = new BidReadDTOExtended();
        bidReadDTO.setId(bid.getId());
        bidReadDTO.setTimeDeparture(bid.getTimeDeparture());
        bidReadDTO.setBidStatus(bid.getBidStatus());
        bidReadDTO.setRoute(bid.getRoute());
        return bidReadDTO;
    }

    public BidReadDTOExtended getBid(UUID id) {
        Bid bid = bidRepository.findById(id).orElseThrow(() ->
                new EntityNotFoundException(Bid.class, id));
        return toReadDTO(bid);
    }

    public BidReadDTO createBid(BidCreateDTO create) {
        Bid bid = new Bid();
        bid.setRoute(create.getRoute());
        bid.setBidStatus(BidStatus.PROCESS);

        TicketReadDTO ticketRead = ticketService.getTicketRouteAndTime(create.getTimeDeparture(), create.getRoute());
        if (ticketRead != null) {
            bid = bidRepository.save(bid);
        } else {
            bid.setId(null);
        }

        BidReadDTO read = new BidReadDTO();
        read.setId(bid.getId());
        return read;
    }

    public BidReadDTOExtended updateBid(UUID id, BidPatchDTO patch) {
        Bid bid = bidRepository.findById(id).get();
        if (patch.getRoute() != null) {
            bid.setRoute(patch.getRoute());
        }
        if (patch.getBidStatus() != null) {
            bid.setBidStatus(patch.getBidStatus());
        }
        bid = bidRepository.save(bid);
        return toReadDTO(bid);
    }

    public BidReadStatusDTO getBidStatus(UUID id) {
        Bid bid = bidRepository.findById(id).orElseThrow(() ->
                new EntityNotFoundException(Bid.class, id));
        BidReadStatusDTO bidStatus = new BidReadStatusDTO();
        bidStatus.setStatus(bid.getBidStatus());
        return bidStatus;
    }

    public List<BidReadDTOExtended> getBidByClientIdAndSort(UUID clientId) {
        Instant instant = Instant.now();
        List<Bid> bidList = bidRepository.bidsByClientIdAndTimeDepartureAfter(clientId, instant);
        bidList.sort(Bid::compareTo);
        List<BidReadDTOExtended> resultList = new ArrayList<>();
        bidList.forEach(o -> {
            resultList.add(toReadDTO(o));
        });
        return resultList;
    }

}
