package com.m.ticket.service.ticketService.enums;

public enum BidStatus {
    PROCESS,
    PROCESSED,
    PROCESSED_MADE,
    BID_ERROR,
}

