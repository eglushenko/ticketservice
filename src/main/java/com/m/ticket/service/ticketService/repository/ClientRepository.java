package com.m.ticket.service.ticketService.repository;


import com.m.ticket.service.ticketService.domin.Client;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface ClientRepository extends CrudRepository<Client, UUID> {
}
