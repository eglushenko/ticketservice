package com.m.ticket.service.ticketService.controller;


import com.m.ticket.service.ticketService.controller.validation.ValidationControllerUtil;
import com.m.ticket.service.ticketService.dto.bid.BidCreateDTO;
import com.m.ticket.service.ticketService.dto.bid.BidReadDTO;
import com.m.ticket.service.ticketService.dto.bid.BidReadDTOExtended;
import com.m.ticket.service.ticketService.dto.bid.BidReadStatusDTO;
import com.m.ticket.service.ticketService.service.BidService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.Instant;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/bid")
public class BidController {

    @Autowired
    private BidService bidService;

    @GetMapping("/{id}")
    public BidReadDTOExtended getBid(@PathVariable UUID id) {
        return bidService.getBid(id);
    }

    @GetMapping("/status/{id}")
    public BidReadStatusDTO getStatusBid(@PathVariable UUID id) {
        return bidService.getBidStatus(id);
    }

    @GetMapping("/client/{id}")
    public List<BidReadDTOExtended> getBidByClientId(@PathVariable UUID id) {
        return bidService.getBidByClientIdAndSort(id);
    }


    @PostMapping
    public BidReadDTO createBid(@RequestBody @Valid BidCreateDTO create) {
        Instant instant = Instant.now();

        ValidationControllerUtil.validateInstantIsAfter(create.getTimeDeparture(), instant);
        return bidService.createBid(create);
    }
}
