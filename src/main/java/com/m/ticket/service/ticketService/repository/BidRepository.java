package com.m.ticket.service.ticketService.repository;

import com.m.ticket.service.ticketService.domin.Bid;
import com.m.ticket.service.ticketService.enums.BidStatus;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;


@Repository
public interface BidRepository extends CrudRepository<Bid, UUID> {

    @Query("select b from Bid b where b.bidStatus = :s1 and b.bidStatus = :s2")
    Stream<UUID> bidIdNotPayed(BidStatus s1, BidStatus s2);

    @Query("select b from Bid  b where b.clientId = :clientId and b.timeDeparture >= :departure")
    List<Bid> bidsByClientIdAndTimeDepartureAfter(UUID clientId, Instant departure);

}
