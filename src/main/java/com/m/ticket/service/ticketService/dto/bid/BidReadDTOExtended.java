package com.m.ticket.service.ticketService.dto.bid;

import com.m.ticket.service.ticketService.enums.BidStatus;
import lombok.Data;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.time.Instant;
import java.util.UUID;


@Data
public class BidReadDTOExtended {

    private UUID id;

    private String route;

    @Enumerated(EnumType.STRING)
    private BidStatus bidStatus;

    private UUID clientId;

    private Instant timeDeparture;

}
