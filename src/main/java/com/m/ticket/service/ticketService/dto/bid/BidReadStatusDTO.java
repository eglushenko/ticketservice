package com.m.ticket.service.ticketService.dto.bid;

import com.m.ticket.service.ticketService.enums.BidStatus;
import lombok.Data;


@Data
public class BidReadStatusDTO {

    private BidStatus status;

}
