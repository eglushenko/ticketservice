package com.m.ticket.service.ticketService.domin;


import com.m.ticket.service.ticketService.enums.BidStatus;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.time.Instant;
import java.util.UUID;

@Getter
@Setter
@Entity
public class Bid extends AbstractEntity implements Comparable<Bid> {

    @NonNull
    private String route;

    @Enumerated(EnumType.STRING)
    private BidStatus bidStatus;

    @NonNull
    private UUID clientId;

    @NonNull
    private Instant timeDeparture;

    @Override
    public int compareTo(Bid bid) {
        if (getTimeDeparture() == null || bid.getTimeDeparture() == null) {
            return 0;
        }
        return getTimeDeparture().compareTo(bid.getTimeDeparture());
    }

}
