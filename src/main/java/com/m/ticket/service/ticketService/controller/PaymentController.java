package com.m.ticket.service.ticketService.controller;


import com.m.ticket.service.ticketService.dto.PayReadDTO;
import com.m.ticket.service.ticketService.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping("/api/pay")
public class PaymentController {

    @Autowired
    private PaymentService paymentService;

    @GetMapping("/{id}")
    public PayReadDTO getPayStatus(@PathVariable UUID id) {
        return paymentService.pay(id);
    }


}
