package com.m.ticket.service.ticketService.exeption.handler;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class ErrorInformation {
    private final HttpStatus status;
    private final Class exceptionClass;
    private final String msg;

    public ErrorInformation(HttpStatus status, Class exceptionClass, String msg) {
        this.status = status;
        this.exceptionClass = exceptionClass;
        this.msg = msg;
    }

}
