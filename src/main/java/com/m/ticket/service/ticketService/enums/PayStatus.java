package com.m.ticket.service.ticketService.enums;

public enum PayStatus {
    ERROR,
    PAYED,
    PENDING,
}
