package com.m.ticket.service.ticketService.job;


import com.m.ticket.service.ticketService.dto.PayReadDTO;
import com.m.ticket.service.ticketService.dto.bid.BidPatchDTO;
import com.m.ticket.service.ticketService.enums.BidStatus;
import com.m.ticket.service.ticketService.enums.PayStatus;
import com.m.ticket.service.ticketService.repository.BidRepository;
import com.m.ticket.service.ticketService.service.BidService;
import com.m.ticket.service.ticketService.service.ExecutorManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;

@Slf4j
@Component
@Transactional(readOnly = true)
public class PaymentProcessingJob {

    @Autowired
    private BidRepository bidRepository;

    @Autowired
    private BidService bidService;

    @Value("${url.pay.api}")
    String urlPay;

    @Transactional(readOnly = true)
    @Scheduled(cron = "${payment.processing.job.cron}")
    public void paymentJob() {
        log.info("Start payment job");

        bidRepository.bidIdNotPayed(BidStatus.PROCESS, BidStatus.PROCESSED_MADE)
                .forEach(b -> {
                    ExecutorManager.executorService.execute(() -> {
                        try {
                            String url = urlPay + b.toString();
                            RestTemplate restTemplate = new RestTemplate();
                            HttpHeaders headers = new HttpHeaders();
                            headers.setContentType(MediaType.APPLICATION_JSON);
                            headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

                            HttpEntity request = new HttpEntity(headers);

                            ResponseEntity<PayReadDTO> response = restTemplate.exchange(
                                    url,
                                    HttpMethod.GET,
                                    request,
                                    PayReadDTO.class
                            );
                            BidPatchDTO update = new BidPatchDTO();
                            if (response.getBody().getPayStatus().equals(PayStatus.ERROR)) {
                                update.setBidStatus(BidStatus.BID_ERROR);
                            }
                            if (response.getBody().getPayStatus().equals(PayStatus.PAYED)) {
                                update.setBidStatus(BidStatus.PROCESSED_MADE);
                            }
                            if (response.getBody().getPayStatus().equals(PayStatus.PENDING)) {
                                update.setBidStatus(BidStatus.PROCESSED);
                            }
                            bidService.updateBid(b, update);

                        } catch (Exception ex) {
                            log.warn(ex.getLocalizedMessage());
                        }
                    });

                });
    }
}
