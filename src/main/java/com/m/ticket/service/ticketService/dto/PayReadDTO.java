package com.m.ticket.service.ticketService.dto;

import com.m.ticket.service.ticketService.enums.PayStatus;
import lombok.Data;

@Data
public class PayReadDTO {

    private PayStatus payStatus;

}
