package com.m.ticket.service.ticketService.service;


import com.m.ticket.service.ticketService.domin.Ticket;
import com.m.ticket.service.ticketService.dto.ticket.TicketReadDTO;
import com.m.ticket.service.ticketService.repository.TicketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;

@Service
public class TicketService {

    @Autowired
    private TicketRepository ticketRepository;

    public TicketReadDTO toReadDTO(Ticket ticket) {
        TicketReadDTO ticketReadDTO = new TicketReadDTO();
        ticketReadDTO.setId(ticket.getId());
        ticketReadDTO.setDeparture(ticket.getDeparture());
        ticketReadDTO.setRouteNumber(ticket.getRouteNumber());
        return ticketReadDTO;
    }

    public TicketReadDTO getTicketRouteAndTime(Instant dataTimeInstant, String route) {
        try {
            Ticket read = ticketRepository
                    .findByDepartureAndRouteNumber(dataTimeInstant, route);
            return toReadDTO(read);
        } catch (NullPointerException ex) {
            return null;
        }

    }
}
