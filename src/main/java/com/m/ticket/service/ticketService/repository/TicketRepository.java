package com.m.ticket.service.ticketService.repository;

import com.m.ticket.service.ticketService.domin.Ticket;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.UUID;

@Repository
public interface TicketRepository extends CrudRepository<Ticket, UUID> {

    Ticket findByDepartureAndRouteNumber(Instant departure, String routeNumber);

}
