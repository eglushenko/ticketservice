package com.m.ticket.service.ticketService.domin;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import java.time.Instant;

@Getter
@Setter
@Entity
public class Ticket extends AbstractEntity {

    private String routeNumber;

    private Instant departure;
}
