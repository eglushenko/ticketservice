package com.m.ticket.service.ticketService.controller.validation;

import com.m.ticket.service.ticketService.exeption.ControllerValidationException;
import lombok.experimental.UtilityClass;

import java.time.Instant;

@UtilityClass
public class ValidationControllerUtil {

    public void validateInstantIsAfter(Instant i1, Instant i2) throws ControllerValidationException {
        if (i1.isBefore(i2)) {
            throw new ControllerValidationException(String.format(" Date %s should be after %s",
                    i1, i2));
        }
    }


}
