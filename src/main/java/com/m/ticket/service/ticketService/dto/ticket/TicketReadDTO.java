package com.m.ticket.service.ticketService.dto.ticket;

import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
public class TicketReadDTO {

    private UUID id;

    private String routeNumber;

    private Instant departure;
}
