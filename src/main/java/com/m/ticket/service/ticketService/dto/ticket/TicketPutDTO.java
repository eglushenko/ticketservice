package com.m.ticket.service.ticketService.dto.ticket;

import lombok.Data;

import java.time.Instant;

@Data
public class TicketPutDTO {

    private String routeNumber;

    private Instant departure;
}
